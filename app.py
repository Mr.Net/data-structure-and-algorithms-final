from classes.CLASS import Question
import time
import csv
import typing
from os import system

clear = lambda: system("clear")


invites_label: list = [
    {
        "greeting message": """
-----------------------------
    Welcome to Quiz game!
-----------------------------
    """,
    },
    {"error message": "Wrong Input!"},
]
question_prompts: list = [
    """
    1, What is (UP) stand for?
    (a) University of Puthisastra
    (b) University of Puthisastras
    (c) University of Puthisastrah
    (d) University  Puthisastrah
    """,
    """
    2, When was UP started up?
    (a) 2007
    (b) 2003
    (c) 2005
    (d) 2002
    """,
    """
    3, How many main major are there in UP?
    (a) 9
    (b) 7
    (c) 3
    (d) 5
    """,
    """
    4, How many subjects are there in ICT field?
    (a) 2, database & programming and newtworking
    (b) 3, database, programming, and newtworking 
    (c) 1, only programming 
    (d) 4, etc
    """,
    """
    5, What are the 3-core values of UP?
    (a) Honor self, Respect other, Develop society
    (b) respect self, respect other, respect society
    (c) respect self, respect staff, respect lecturer
    (d) honor parent, honor lecturer, honor society
    """,
]


user_data: dict = {}
mulptiple_choise: list = ["a", "b", "c", "d"]
choice_hint: str = "(a), (b), (c), (d)"

questions: list = [
    Question(question_prompts[0], "a"),
    Question(question_prompts[1], "a"),
    Question(question_prompts[2], "a"),
    Question(question_prompts[3], "a"),
    Question(question_prompts[4], "a"),
]


get_correct_input: bool = False
loop_start: bool = True
print(invites_label[0]["greeting message"])

# corordinator: str = input("Corodinator name: ")
# corordinator.capitalize()
# while not get_correct_input:
#     try:
#         num_question: int = input("How many questions you need? ")
#         break
#     except:
#         print(invites_label[1]["error message"])

# index_question: int = 1
# for num in range(1, num_question + 1):
#     question_input: str = input("question {}: ".format(num))
#     answer_input: str = input("Answer {}: ".format(num))
#     # index_question += 1


print("How many people are playing? (1-4)")
while not get_correct_input:
    try:
        while loop_start:
            players = int(input("Input here: "))
            if players in range(1, 5):
                break
            else:
                print("Please enter 1 to 4!")
                continue
        break
    except:
        print("Please Input a valid number!")

list_player: list = []
indext_player: int = 0
for player in range(1, players + 1):
    indext_player += 1
    name: str = input("player {}: ".format(indext_player))
    name = name.capitalize()
    user_data[name] = 0


def run_quiz(questions):
    score: int = 0

    for question in questions:
        for player_name, player_score in user_data.items():
            print(question.prompt)

            while not get_correct_input:
                # answer = None
                answer: str = input("{}: ".format(player_name))
                answer.lower()

                if answer in mulptiple_choise:
                    break
                else:
                    print(invites_label[1]["error message"])
                    print("Choose: {}".format(choice_hint))
                    continue
            if answer == question.answer:
                # score += 1
                user_data[player_name] = player_score + 1
            time.sleep(0.5)
            clear()


run_quiz(questions)

# out put each player's score
indext_score: int = 0
print(
    """
    \t=============================
    \t        Score Board
    \t=============================
"""
)

for key, value in user_data.items():
    indext_score += 1

    print("\t  {}| {} | get {} point(s)".format(indext_score, key, value))
    print("\t-----------------------------\n")


# export score board to csv file
with open("player_scores_board.csv", "w", newline="") as file:
    fieldnames: list = ["player name", "score"]
    writer = csv.writer(file, fieldnames)

    writer.writerow(fieldnames)

    for data_name, data_score in user_data.items():
        writer.writerow([data_name, data_score])
